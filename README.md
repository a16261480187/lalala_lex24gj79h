# 大小单双买法技巧

#### 介绍
大小单双买法技巧【🐧Q: 3️⃣2️⃣8️⃣5️⃣2️⃣7️⃣3️⃣ ★倌罔：xs6️⃣0️⃣8️⃣.cc】生活幸福和苦难，本就是一对孪生姐妹，别去一味埋怨，学会适应，然后用一颗豁达的心，快乐的去靠近幸福，因为每一个人都有属于自己的幸福，幸福的样子大抵相同，那就是懂得知足。
把快乐握在手中，将幸福放在心间，幸福从来就不在远方，就在你的手边，在你的柴米油盐酱醋茶里，只是你只顾着观望别人的幸福，而忽略了自己已拥有的幸福罢了。充满自信，做最好的自己

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
